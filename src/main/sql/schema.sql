
CREATE DATABASE IF NOT  EXISTS temp;
USE temp;
-- ——————————————————————————
-- create table sys_user_temp
-- add by  wujiawei  2022-01-25
-- ——————————————————————————
DROP TABLE IF EXISTS `sys_user_temp`;
CREATE TABLE `sys_user_temp` (
                                 username  varchar(255)  COMMENT '',
                                 password  varchar(255)  COMMENT 'null',
                                 scope  varchar(255)  COMMENT '',
                                 `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                                 `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
                                 `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                 `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='';
-- ------end
-- ——————————————————————————