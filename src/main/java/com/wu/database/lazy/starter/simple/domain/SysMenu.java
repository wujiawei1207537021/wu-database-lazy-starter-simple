package com.wu.database.lazy.starter.simple.domain;

import lombok.Data;
import org.wu.framework.lazy.orm.core.stereotype.LazyTable;
import org.wu.framework.lazy.orm.core.stereotype.LazyTableFieldId;


@LazyTable(tableName = "sys_menu",comment = "系统菜单")
@Data
public class SysMenu {
    @LazyTableFieldId
    private Integer id;
    private String name;
    private String url;
    private String icon;
    private Integer parentId;
    private String parentName;
    private Integer sort;
    private Integer status;

}
