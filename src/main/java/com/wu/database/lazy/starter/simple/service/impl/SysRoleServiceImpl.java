package com.wu.database.lazy.starter.simple.service.impl;

import com.wu.database.lazy.starter.simple.mapper.SysRoleLazyJpaRepository;
import com.wu.database.lazy.starter.simple.service.SysRoleService;
import org.springframework.stereotype.Service;

@Service
public class SysRoleServiceImpl implements SysRoleService {

    private final SysRoleLazyJpaRepository sysRoleLazyJpaRepository;

    public SysRoleServiceImpl(SysRoleLazyJpaRepository sysRoleLazyJpaRepository) {
        this.sysRoleLazyJpaRepository = sysRoleLazyJpaRepository;
    }

}
