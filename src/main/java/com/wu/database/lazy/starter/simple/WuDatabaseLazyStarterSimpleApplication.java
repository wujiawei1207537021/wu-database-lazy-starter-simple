package com.wu.database.lazy.starter.simple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.wu.framework.inner.lazy.orm.spring.annotation.LazyRepositoryScan;
import org.wu.framework.lazy.orm.core.stereotype.LazyScan;


@EnableScheduling
@LazyScan(scanBasePackages = "com.wu.database.lazy.starter.simple.domain")
@LazyRepositoryScan(scanBasePackages = "com.wu.database.lazy.starter.simple.mapper")
@SpringBootApplication
public class WuDatabaseLazyStarterSimpleApplication {

    public static void main(String[] args) {
        SpringApplication.run(WuDatabaseLazyStarterSimpleApplication.class, args);
    }
}
