package com.wu.database.lazy.starter.simple.controller;

import com.wu.database.lazy.starter.simple.domain.SysUserBatch;
import com.wu.database.lazy.starter.simple.service.SysUserBatchService;
import com.wu.database.lazy.starter.simple.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.PostConstruct;
import org.springframework.web.bind.annotation.PostMapping;
import org.wu.framework.web.spring.EasyController;


import java.util.Arrays;

@Tag(name = "批量操作")
@EasyController("/batch")
public class BatchController {
    private final SysUserService sysUserService;
    private final SysUserBatchService sysUserBatchService;

    public BatchController(SysUserService sysUserService, SysUserBatchService sysUserBatchService) {
        this.sysUserService = sysUserService;
        this.sysUserBatchService = sysUserBatchService;
    }


    /**
     * 用户信息简单插入
     */
//    @PostConstruct
    @Operation(summary ="用户信息简单插入")
    @PostMapping("/mybatis/batch")
    public void mybatisBatch() {
        SysUserBatch sysUserBatch = new SysUserBatch();
        sysUserBatch.setId(2024L);
        sysUserBatchService.saveBatch(Arrays.asList(sysUserBatch));
    }

}
