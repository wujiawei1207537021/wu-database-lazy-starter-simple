package com.wu.database.lazy.starter.simple.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wu.database.lazy.starter.simple.domain.SysUser;

public interface SysUserService extends IService<SysUser> {
}
