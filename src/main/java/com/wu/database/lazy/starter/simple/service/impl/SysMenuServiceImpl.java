package com.wu.database.lazy.starter.simple.service.impl;

import com.wu.database.lazy.starter.simple.domain.SysMenu;
import com.wu.database.lazy.starter.simple.mapper.SysMenuLazyJpaRepository;
import com.wu.database.lazy.starter.simple.service.SysMenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;

import java.util.List;
import java.util.Map;


@Slf4j
@Service
public class SysMenuServiceImpl implements SysMenuService {

    private final SysMenuLazyJpaRepository sysMenuLazyJpaRepository;

    public SysMenuServiceImpl(SysMenuLazyJpaRepository sysMenuLazyJpaRepository) {
        this.sysMenuLazyJpaRepository = sysMenuLazyJpaRepository;
    }

    /**
     * 新增菜单
     *
     * @param sysMenu menu
     * @return Result
     */
    @Override
    public Result<?> save(SysMenu sysMenu) {
        sysMenuLazyJpaRepository.save(sysMenu);
        return ResultFactory.successOf();
    }

    /**
     * 获取菜单分页
     *
     * @param sysMenu 查询条件
     * @return 返回分页结果
     */
    @Override
    public Result<LazyPage<?>> page(SysMenu sysMenu) {
        Iterable<SysMenu> all = sysMenuLazyJpaRepository.findAll();
        return ResultFactory.successOf(LazyPage.of(1, 10, List.of(all)));
    }

    /**
     * 获取所有菜单数据
     *
     * @return 返回所有菜单数据
     */
    @Override
    public Result<List<?>> findAll() {
        return ResultFactory.successOf(List.of(sysMenuLazyJpaRepository.findAll()));
    }

    /**
     * 判断数据是否存在
     *
     * @param sysMenu 判断条件
     * @return 是否存在
     */
    @Override
    public Result<Boolean> exist(SysMenu sysMenu) {
        return ResultFactory.successOf(sysMenuLazyJpaRepository.existsById(sysMenu.getId()));
    }

    /**
     * 统计数据
     *
     * @param sysMenu 统计条件
     * @return 统计数据
     */
    @Override
    public Result<Long> count(SysMenu sysMenu) {
        return ResultFactory.successOf(sysMenuLazyJpaRepository.count());
    }
    /**
     * 测试JPA
     *
     * @return 结果
     */
    @Override
    public Result<?> testJpa() {


        // 新增
        sysMenuLazyJpaRepository.insert(100, "菜单管理", "www.baidu.com", "/icon.png", 0, "系统", 2, 1);
        // 修改
        sysMenuLazyJpaRepository.updateNameById(100, "修改后菜单管理");


        // 查询
        // 自定义查询单个数据
        SysMenu menuById = sysMenuLazyJpaRepository.findMenuById(100);
        log.info("findMenuById:{}", menuById);
        // 自定义查询List 数据
        List<SysMenu> listByParentId = sysMenuLazyJpaRepository.findListByParentId(0);
        log.info("findListByParentId:{}", listByParentId);
        // 自定义分页查询
        // 自定义 List map查询
        List<Map<?, ?>> listMapByParentId = sysMenuLazyJpaRepository.findListMapByParentId(0);
        log.info("findListMapByParentId:{}", listMapByParentId);


        // 获取数据
        SysMenu findById = sysMenuLazyJpaRepository.findById(100);
        log.info("findById:{}", findById);

        // 获取ID、名称、url、icon
        SysMenu idAndNameAndUrlAndIconById = sysMenuLazyJpaRepository.findIdAndNameAndUrlAndIconById(100);
        log.info("idAndNameAndUrlAndIconById:{}", idAndNameAndUrlAndIconById);
        // 删除
        sysMenuLazyJpaRepository.removeById(100);

        boolean existsById = sysMenuLazyJpaRepository.existsById(100);
        log.info("existsById:{}", existsById);

        return ResultFactory.successOf();
    }
}
