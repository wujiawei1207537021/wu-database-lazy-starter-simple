package com.wu.database.lazy.starter.simple.mapper;

import com.wu.database.lazy.starter.simple.domain.SysMenu;
import org.apache.ibatis.annotations.Update;
import org.wu.framework.lazy.orm.database.jpa.repository.LazyJpaRepository;
import org.wu.framework.lazy.orm.database.jpa.repository.annotation.*;

import java.util.List;
import java.util.Map;


@LazyRepository
public interface SysMenuLazyJpaRepository extends LazyJpaRepository<SysMenu, Integer> {


    // TODO 适配
    List<SysMenu> findByPid(Integer pid);

    @LazySelect("select * from sys_menu where parent_id={parentId}")
    List<SysMenu> findListByParentId(Integer parentId);


    @LazySelect("select * from sys_menu where id ={id}")
    SysMenu findMenuById(@LazyParam("id") Integer menuId);


    @LazySelect("select * from sys_menu where id ={id}")
    List<Map<?, ?>> findListMapByParentId(@LazyParam("id") Integer menuId);

    @LazyInsert("INSERT Ignore INTO `sys_menu` ( `id`, `name`, `url`, `icon`, `parent_id`,`parent_name`,`sort`,`status`) VALUES ({id},{name},{url},{icon},{parentId},{parentName},{sort},{status});")
    void insert(Integer id,
                String name,
                String url,
                String icon,
                Integer parentId,
                String parentName,
                Integer sort,
                Integer status
    );

    @LazyUpdate("UPDATE `sys_menu` SET  `name` = {name} WHERE `id` = {id};")
    void updateNameById(Integer id, String name);


    @LazyDelete("DELETE FROM sys_menu  WHERE `id` = {id};")
    void removeById(int id);

    /**
     * 执行sql select * from sys_menu where id={id}
     * 根据ID获取数据
     * @param id 数据ID
     */
    SysMenu findById(int id);

    /**
     * 执行sql select id,name,url,icon from sys_menu where id={id}
     * 获取ID、名称、url、icon
     * @param id 主键ID
     * @return 查询信息
     */
    SysMenu findIdAndNameAndUrlAndIconById(int id);


}
