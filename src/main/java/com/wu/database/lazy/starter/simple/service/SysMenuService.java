package com.wu.database.lazy.starter.simple.service;

import com.wu.database.lazy.starter.simple.domain.SysMenu;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;

import java.util.List;

public interface SysMenuService {

    /**
     * 新增菜单
     *
     * @param sysMenu menu
     * @return Result
     */

    Result<?> save(SysMenu sysMenu);

    /**
     * 获取菜单分页
     *
     * @param sysMenu 查询条件
     * @return 返回分页结果
     */
    Result<LazyPage<?>> page(SysMenu sysMenu);

    /**
     * 获取所有菜单数据
     * @return 返回所有菜单数据
     */
    Result<List<?>> findAll();

    /**
     *
     * 判断数据是否存在
     * @param sysMenu 判断条件
     * @return 是否存在
     */
    Result<Boolean> exist(SysMenu sysMenu);

    /**
     * 统计数据
     *
     * @param sysMenu 统计条件
     * @return 统计数据
     */
    Result<Long> count(SysMenu sysMenu);

    /**
     * 测试JPA
     * @return 结果
     */
    Result<?> testJpa();
}
