package com.wu.database.lazy.starter.simple.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wu.database.lazy.starter.simple.domain.SysUserBatch;
import com.wu.database.lazy.starter.simple.mapper.SysUserBatchMapper;
import com.wu.database.lazy.starter.simple.service.SysUserBatchService;
import org.springframework.stereotype.Service;

/**
 * description
 *
 * @author 吴佳伟
 * @date 2023/03/01 11:38
 */
@Service
public class SysUserBatchServiceImpl extends ServiceImpl<SysUserBatchMapper, SysUserBatch> implements SysUserBatchService {
}
