package com.wu.database.lazy.starter.simple.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wu.database.lazy.starter.simple.domain.SysUser;
import com.wu.database.lazy.starter.simple.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.lazy.orm.database.lambda.stream.condition.LambdaBasicComparison;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import org.wu.framework.web.spring.EasyController;

/**
 * describe :
 *
 * @author : Jia wei Wu
 * @version 1.0
 * @date : 2022/9/18 17:46
 */
@Tag(name = "查询案例")
@EasyController("/select")
public class SelectController {

    private final LazyLambdaStream lazyLambdaStream;
    private final SysUserService sysUserService;


    public SelectController(LazyLambdaStream lazyLambdaStream, SysUserService sysUserService) {
        this.lazyLambdaStream = lazyLambdaStream;
        this.sysUserService = sysUserService;
    }


    /**
     * description: mybatis 单表查询
     * @return
     * @author 吴佳伟
     * @date: 16.1.23 13:09
     */
    @Operation(summary ="mybatis单表查询(service)")
    @GetMapping("/simple/mybatis/service/select")
    public Result<SysUser> mybatisServiceSelect() {
        SysUser one = sysUserService.getOne(Wrappers.<SysUser>lambdaQuery()
                .eq(SysUser::getIsDeleted, false));
        return ResultFactory.successOf(one);
    }
    /**
     * description: mybatis 单表查询
     *
     * @return
     * @author 吴佳伟
     * @date: 16.1.23 13:09
     */
    @Operation(summary ="mybatis单表查询(Model)")
    @GetMapping("/simple/mybatis/model/select")
    public Result<SysUser> mybatisModelSelect() {
        SysUser sysUser = new SysUser();
        SysUser one = sysUser.selectOne(Wrappers.<SysUser>lambdaQuery()
                .eq(SysUser::getIsDeleted, false));
        return ResultFactory.successOf(one);
    }

    /**
     * 执行sql
     * select temp.sys_user_temp.* from temp.sys_user_temp where  sys_user_temp.id  =  1  limit 1
     *
     * @return
     */
    @Operation(summary ="单表查询")
    @GetMapping("/simple/select")
    public Result<SysUser> lazySelect() {

        LambdaBasicComparison<SysUser> sysUserLambdaBasicComparison = LazyWrappers.lambdaWrapper();
        sysUserLambdaBasicComparison.eq(SysUser::getId, 1L)
                .limit(1L);
        SysUser sysUser = lazyLambdaStream.selectOne(sysUserLambdaBasicComparison);
        return ResultFactory.successOf(sysUser);
    }

    /**
     * 执行sql
     *
     * @return
     */
    @Operation(summary ="单表分页查询")
    @GetMapping("/simple/select/page")
    public Result<LazyPage<SysUser>> lazySelectPage() {
        LazyPage<SysUser> page = new LazyPage<>(1, 10);
        LambdaBasicComparison<SysUser> sysUserLambdaBasicComparison = LazyWrappers.lambdaWrapper();
        sysUserLambdaBasicComparison.eq(SysUser::getId, 1L)
                .limit(1L);
        LazyPage<SysUser> sysUserPage = lazyLambdaStream.selectPage(sysUserLambdaBasicComparison, page);
        return ResultFactory.successOf(sysUserPage);
    }


}
