package com.wu.database.lazy.starter.simple.domain;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.wu.framework.lazy.orm.core.stereotype.LazyTable;
import org.wu.framework.lazy.orm.core.stereotype.LazyTableField;
import org.wu.framework.lazy.orm.core.stereotype.LazyTableFieldId;

import java.util.Date;

/**
 *  临时用户
 */
@Data
@Accessors(chain = true)
@LazyTable(tableName = "sys_user_temp")
@TableName("sys_user_temp")
@ApiModel(value = "sys_user",description = "临时用户")
public class SysUser extends Model<SysUser> {

    /**
     * 
     * 
     */
    @ApiModelProperty(name ="username",example = "")
    @LazyTableField(name ="username",comment = "")
    private String username;
    /**
     * 
     * null
     */
    @ApiModelProperty(name ="password",example = "")
    @LazyTableField(name ="password",comment = "null")
    private String password;
    /**
     * 
     * 
     */
    @ApiModelProperty(name ="scope",example = "")
    @LazyTableField(name ="scope",comment = "")
    private String scope;
    /**
     * 
     * 主键
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(name ="id",example = "")
    @LazyTableFieldId(name ="id",comment = "主键")
    private Long id;
    /**
     * 
     * 是否删除
     */
    @ApiModelProperty(name ="isDeleted",example = "")
    @LazyTableField(name ="is_deleted",comment = "是否删除")
    private Integer isDeleted;
    /**
     * 
     * 创建时间
     */
    @ApiModelProperty(name ="createTime",example = "")
    @LazyTableField(name ="create_time",comment = "创建时间")
    private Date createTime;
    /**
     * 
     * 更新时间
     */
    @ApiModelProperty(name ="updateTime",example = "")
    @LazyTableField(name ="update_time",comment = "更新时间")
    private Date updateTime;
}