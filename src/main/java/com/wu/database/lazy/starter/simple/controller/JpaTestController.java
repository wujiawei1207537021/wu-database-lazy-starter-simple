package com.wu.database.lazy.starter.simple.controller;


import com.wu.database.lazy.starter.simple.service.SysMenuService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.spring.EasyController;


@Tag(name = "测试Lazy Jpa")
@EasyController("/lazy/jpa")
public class JpaTestController {


    private final SysMenuService sysMenuService;

    public JpaTestController(SysMenuService sysMenuService) {
        this.sysMenuService = sysMenuService;
    }

    /**
     * 测试JPA
     *
     * @return 子节点菜单数据
     */
    @Operation(summary = "测试JPA")
    @GetMapping("/menu/testJpa")
    public Result<?> testJpa() {
        return sysMenuService.testJpa();
    }
}
