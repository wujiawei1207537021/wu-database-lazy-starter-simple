package com.wu.database.lazy.starter.simple.controller;

import com.wu.database.lazy.starter.simple.domain.SysUser;
import com.wu.database.lazy.starter.simple.service.SysUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import org.wu.framework.web.spring.EasyController;

import java.util.ArrayList;
import java.util.List;

/**
 * description Upsert 使用
 *
 * @author 吴佳伟
 * @company 
 * @date 2022/1/25$ 1:36 下午$
 */
@Tag(name = "Upsert 使用")
@EasyController("/upsert")
public class UpsertController {

    private final LazyLambdaStream lazyLambdaStream;
    private final SysUserService sysUserService;

    public UpsertController(LazyLambdaStream lazyLambdaStream, SysUserService sysUserService) {
        this.lazyLambdaStream = lazyLambdaStream;
        this.sysUserService = sysUserService;
    }

    /**
     * 用户信息简单插入
     */
    @Operation(summary ="用户信息简单插入")
    @PostMapping("/lazy/upsert")
    public void lazyUpsert() {
        SysUser sysUser = new SysUser();
        sysUser.setUsername("小来");
        sysUser.setPassword("1234");
        sysUser.setId(1L);
        lazyLambdaStream.upsert(sysUser);
    }


    /**
     * 用户信息mybatis插入
     */
    @Deprecated
    @Operation(summary ="用户信息mybatis插入")
    @PostMapping("/upsert")
    public void mybatisUpsert() {
        final SysUser byId = sysUserService.getById(1L);
        if(ObjectUtils.isEmpty(byId)){
            SysUser sysUser = new SysUser();
            sysUser.setUsername("小来-upsert");
            sysUser.setPassword("1234");
            sysUser.setId(1L);
            sysUser.insert();
        }else {
            SysUser sysUser = new SysUser();
            sysUser.setUsername("小来-upsert");
            sysUser.setPassword("1234");
            sysUser.setId(1L);
            sysUserService.updateById(sysUser);
        }

    }

    /**
     * 用户信息简单批量插入
     */
    @Operation(summary ="用户信息简单批量插入")
    @PostMapping("/lazy/upsert/batch")
    public void lazyBatchUpsert() {
        lazyLambdaStream.upsert(createList("lazy-upsert"));
    }

    /**
     * 用户信息mybatis 批量插入 // TODO 先查询后更新
     */
    @Deprecated
    @Operation(summary ="用户信息mybatis 批量插入")
    @PostMapping("/upsert/batch")
    public void mybatisBatchUpsert() {
        for (SysUser mybatis : createList("mybatis-upsert")) {
            final SysUser byId = sysUserService.getById(mybatis.getId());
            if(ObjectUtils.isEmpty(byId)){
                mybatis.insert();
            }else {
                sysUserService.updateById(mybatis);
            }
        }
    }


    /**
     * 创建用户list
     *
     * @return
     */
    protected List<SysUser> createList(String scope) {
        List<SysUser> sysUserList = new ArrayList<>();
        for (Long i  = 0L; i < 1000; i++) {
            SysUser sysUser = new SysUser();
            sysUser.setUsername("小来" + 1);
            sysUser.setPassword("1234");
            sysUser.setScope(scope);
            sysUser.setId(i);
            sysUserList.add(sysUser);
        }
        return sysUserList;

    }


}
