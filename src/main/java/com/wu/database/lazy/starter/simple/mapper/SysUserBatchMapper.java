package com.wu.database.lazy.starter.simple.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wu.database.lazy.starter.simple.domain.SysUserBatch;
import org.apache.ibatis.annotations.Mapper;

/**
 * description
 *
 * @author 吴佳伟
 * @date 2023/03/01 11:39
 */
@Mapper
public interface SysUserBatchMapper extends BaseMapper<SysUserBatch> {
}
