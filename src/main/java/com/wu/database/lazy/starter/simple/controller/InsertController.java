package com.wu.database.lazy.starter.simple.controller;

import com.wu.database.lazy.starter.simple.domain.SysUser;
import com.wu.database.lazy.starter.simple.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.PostMapping;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import org.wu.framework.web.spring.EasyController;

import java.util.ArrayList;
import java.util.List;

/**
 * description insert 使用
 *
 * @author 吴佳伟
 * @company 
 * @date 2022/1/25$ 1:36 下午$
 */
@Tag(name = "insert 使用")
@EasyController("/insert")
public class InsertController {

    private final LazyLambdaStream lazyLambdaStream;
    private final SysUserService sysUserService;

    public InsertController(LazyLambdaStream lazyLambdaStream, SysUserService sysUserService) {
        this.lazyLambdaStream = lazyLambdaStream;
        this.sysUserService = sysUserService;
    }

    /**
     * 用户信息简单插入
     */
    @Operation(summary ="用户信息简单插入")
    @PostMapping("/lazy/insert")
    public void lazyInsert() {
        SysUser sysUser = new SysUser();
        sysUser.setUsername("小来");
        sysUser.setPassword("1234");
        lazyLambdaStream.insert(sysUser);
    }

    /**
     * 用户信息简单批量插入
     */
    @Operation(summary ="用户信息简单批量插入")
    @PostMapping("/lazy/insert/batch")
    public void lazyBatchInsert() {
        lazyLambdaStream.insert(createList("lazy"));
    }

    /**
     * 用户信息mybatis插入
     */
    @Operation(summary ="用户信息mybatis插入")
    @PostMapping("/insert")
    public void mybatisInsert() {
        SysUser sysUser = new SysUser();
        sysUser.setUsername("小来");
        sysUser.setPassword("1234");
        sysUser.insert();
    }

    /**
     * 用户信息mybatis 批量插入
     */
    @Operation(summary ="用户信息mybatis 批量插入")
    @PostMapping("/insert/batch")
    public void mybatisBatchInsert() {
        sysUserService.saveBatch(createList("mybatis"));
    }


    /**
     * 创建用户list
     *
     * @return
     */
    protected List<SysUser> createList(String scope) {
        List<SysUser> sysUserList = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            SysUser sysUser = new SysUser();
            sysUser.setUsername("小来" + 1);
            sysUser.setPassword("1234");
            sysUser.setScope(scope);
            sysUserList.add(sysUser);
        }
        return sysUserList;

    }


}
