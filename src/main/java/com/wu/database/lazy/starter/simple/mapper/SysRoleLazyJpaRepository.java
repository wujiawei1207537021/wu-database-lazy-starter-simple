package com.wu.database.lazy.starter.simple.mapper;

import com.wu.database.lazy.starter.simple.domain.SysRole;
import org.wu.framework.lazy.orm.database.jpa.repository.LazyJpaRepository;
import org.wu.framework.lazy.orm.database.jpa.repository.annotation.LazyRepository;

@LazyRepository
public interface SysRoleLazyJpaRepository extends LazyJpaRepository<SysRole, Integer> {
}
