package com.wu.database.lazy.starter.simple.scheduled;

import com.wu.database.lazy.starter.simple.domain.SysRole;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.wu.framework.core.utils.DataTransformUntil;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class MysqlScheduling {

    private final LazyLambdaStream lazyLambdaStream;

    public MysqlScheduling(LazyLambdaStream lazyLambdaStream) {
        this.lazyLambdaStream = lazyLambdaStream;
    }


    /**
     * 2秒扫描一次数据库
     * 扫描需要确认数据 未确认数据将重新发送
     */
    @Scheduled(fixedRate = 1000 * 2)
    public void schedulingMySQL() {

        int num=10;
        List<SysRole> sysRoles = DataTransformUntil.simulationBeanList(SysRole.class, num);

        // 保存数据
        lazyLambdaStream.insert(sysRoles);
        List<Integer> idList = sysRoles.stream().map(SysRole::getId).toList();
        // 查询数据
        Long count = lazyLambdaStream.count(LazyWrappers.<SysRole>lambdaWrapper().in(SysRole::getId, idList));
        if(count!=num){
            log.error("--------------------------------------查询出数据:{},期望数据:{}",count,num);
        }else {
            log.info("--------------------------------------执行成功");
        }

        // 删除数据
        lazyLambdaStream.delete(LazyWrappers.<SysRole>lambdaWrapper().in(SysRole::getId, idList));
    }
}
