package com.wu.database.lazy.starter.simple.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wu.database.lazy.starter.simple.domain.SysUserBatch;

/**
 * description
 *
 * @author 吴佳伟
 * @date 2023/03/01 11:38
 */
public interface SysUserBatchService extends IService<SysUserBatch> {
}
