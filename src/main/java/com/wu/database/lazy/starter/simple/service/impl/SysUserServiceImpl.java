package com.wu.database.lazy.starter.simple.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wu.database.lazy.starter.simple.domain.SysUser;
import com.wu.database.lazy.starter.simple.mapper.SysUserMapper;
import com.wu.database.lazy.starter.simple.service.SysUserService;
import org.springframework.stereotype.Service;

/**
 * description
 *
 * @author 吴佳伟
 * @company 
 * @date 2022/1/25$ 3:05 下午$
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

}
