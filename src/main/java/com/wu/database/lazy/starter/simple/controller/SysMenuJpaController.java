package com.wu.database.lazy.starter.simple.controller;

import com.wu.database.lazy.starter.simple.domain.SysMenu;
import com.wu.database.lazy.starter.simple.service.SysMenuService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.spring.EasyController;

import java.util.List;

@Tag(name = "系统菜单管理 Lazy Jpa")
@EasyController("/lazy/sys/menu/jpa")
public class SysMenuJpaController {

    private final SysMenuService sysMenuService;

    public SysMenuJpaController(SysMenuService sysMenuService) {
        this.sysMenuService = sysMenuService;
    }

    /**
     * 新增菜单
     */
    @Operation(summary = "新增菜单")
    @PostMapping("/menu/save")
    public Result<?> save(@RequestBody SysMenu sysMenu) {
        return sysMenuService.save(sysMenu);
    }

    /**
     * 获取菜单分页
     *
     * @param sysMenu 查询条件
     * @return 返回分页结果
     */

    @Operation(summary ="获取菜单分页")
    @GetMapping("/menu/page")
    public Result<LazyPage<?>> page(@ModelAttribute SysMenu sysMenu) {
        return sysMenuService.page(sysMenu);
    }

    /**
     * 获取所有菜单数据
     *
     * @return 返回所有菜单数据
     */

    @Operation(summary ="获取所有菜单数据")
    @GetMapping("/menu/findAll")
    public Result<List<?>> findAll() {
        return sysMenuService.findAll();
    }

    /**
     * 判断数据是否存在
     *
     * @param sysMenu 判断条件
     * @return 是否存在
     */

    @Operation(summary ="判断数据是否存在")
    @PatchMapping("/menu/exist")
    public Result<Boolean> exist(@ModelAttribute SysMenu sysMenu) {
        return sysMenuService.exist(sysMenu);
    }

    /**
     * 统计数据
     *
     * @param sysMenu 统计条件
     * @return 统计数据
     */

    @Operation(summary ="获取菜单分页")
    @PatchMapping("/menu/count")
    public Result<Long> count(@ModelAttribute SysMenu sysMenu) {
        return sysMenuService.count(sysMenu);
    }
    


}
