package com.wu.database.lazy.starter.simple.domain;

import lombok.Data;
import org.wu.framework.lazy.orm.core.stereotype.LazyTable;
import org.wu.framework.lazy.orm.core.stereotype.LazyTableFieldId;

@LazyTable(tableName = "sys_role",comment = "系统角色")
@Data
public class SysRole {

    private Integer id;
    private String roleName;
    private String roleDesc;

    @LazyTableFieldId
    private Integer rid;

}
