package com.wu.database.lazy.starter.simple;

import com.wu.database.lazy.starter.simple.domain.SysUser;
import com.wu.framework.inner.lazy.persistence.converter.SQLConverter;


/**
 * description
 *
 * @author 吴佳伟
 * @company 
 * @date 2022/1/25$ 3:14 下午$
 */
public class SQLCreateTest {
    public static void main(String[] args) {
        SQLConverter.creatTableSQL(SysUser.class);
    }
}
